﻿using DAL;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL
{
    public interface IClientService
    {
        bool AddClient(ClientDTO client);
        ClientDTO GetClientDetail(int id);
        IEnumerable<ClientDTO> GetClients();
        bool UpdateClient(ClientDTO client);
        bool DeleteClient(int id);
    }

    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepo;

        public ClientService(IClientRepository clientRepo)
        {
            _clientRepo = clientRepo;
        }

        public bool AddClient(ClientDTO client)
        {
            //Manual mapping
            var clientToAdd = new Client()
            {
                FirstName = client.FirstName,
                LastName = client.LastName,
                StreetNumber = client.StreetNumber,
                StreetName = client.StreetName,
                City = client.City,
                PostCode = client.PostCode,
                Country = client.Country
            };

           return _clientRepo.AddClient(clientToAdd);
        }

        public ClientDTO GetClientDetail(int id)
        {
            var client = _clientRepo.GetClientDetail(id);
            ClientDTO clientDTO = null;

            if (client != null)
            {
                clientDTO = new ClientDTO()
                {
                    Id = client.Id,
                    FirstName = client.FirstName,
                    LastName = client.LastName,
                    StreetNumber = client.StreetNumber,
                    StreetName = client.StreetName,
                    City = client.City,
                    PostCode = client.PostCode,
                    Country = client.Country
                };
            }


            return clientDTO;
        }

        public IEnumerable<ClientDTO> GetClients()
        {
            var clients = _clientRepo.GetClients();

            if (clients != null)
            {
                var result = clients.AsQueryable().Select(r => new ClientDTO()
                {
                    Id = r.Id,
                    FirstName = r.FirstName,
                    LastName = r.LastName,
                    StreetNumber = r.StreetNumber,
                    StreetName = r.StreetName,
                    City = r.City,
                    PostCode = r.PostCode,
                    Country = r.Country
                }).ToList();

                return result;
            }
            else
            {
                return null;
            }

            
        }

        public bool UpdateClient(ClientDTO client)
        {
            var clientToUpdate = new Client()
            {
                Id = client.Id,
                FirstName = client.FirstName,
                LastName = client.LastName,
                StreetNumber = client.StreetNumber,
                StreetName = client.StreetName,
                City = client.City,
                PostCode = client.PostCode,
                Country = client.Country
            };

            return _clientRepo.UpdateClient(clientToUpdate);
        }

        public bool DeleteClient(int id)
        {
            return _clientRepo.DeleteClient(id);
        }
    }
}
