﻿using BLL;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/Clients")]
    public class ClientsController : ApiController
    {
        private readonly IClientService _clientService;

        public ClientsController(IClientService clientService)
        {
            _clientService = clientService;
        }

        // test 
        // GET: api/Clients
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(List<ClientDTO>))]
        public IHttpActionResult GetClients()
        {
            var clients = _clientService.GetClients();

            if (clients == null)
            {
                return NotFound();
            }

            return Ok(clients);
        }


        // GET: api/Clients/5
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(ClientDTO))]
        public IHttpActionResult GetClientDetail(int id)
        {
            var client = _clientService.GetClientDetail(id);

            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }

        // POST: api/Clients
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(ClientDTO))]
        public IHttpActionResult PostClient(ClientDTO client)
        {
            var result = _clientService.AddClient(client);

            return CreatedAtRoute("DefaultApi", new { controller = "Clients", id = client.Id }, client);

        }

        // PUT: api/Clients/5
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutClient(int id, ClientDTO client)
        {
            if (id != client.Id)
            {
                return BadRequest();
            }

            try
            {
                _clientService.UpdateClient(client);
            }
            catch
            {
                if (_clientService.GetClientDetail(id) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Clients/5
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(ClientDTO))]
        public IHttpActionResult DeleteClient(int id)
        {
            var client = _clientService.GetClientDetail(id);

            if (client == null)
            {
                return NotFound();
            }

            _clientService.DeleteClient(id);

            return Ok(client);
        }
    }
}
