﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IClientRepository
    {
        bool AddClient(Client client);
        Client GetClientDetail(int id);
        List<Client> GetClients();
        bool UpdateClient(Client client);
        bool DeleteClient(int id);
    }

    public class ClientRepository : IClientRepository
    {
        public bool AddClient(Client client)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@FirstName", client.FirstName),
                new SqlParameter("@LastName", client.LastName),
                new SqlParameter("@StreetNumber", client.StreetNumber),
                new SqlParameter("@StreetName", client.StreetName),
                new SqlParameter("@City", client.City),
                new SqlParameter("@PostCode", client.PostCode),
                new SqlParameter("@Country", client.Country)
            };

            return SQLHelper.ExecuteNonQuery("AddClient", CommandType.StoredProcedure, parameters);
        }

        public Client GetClientDetail(int id)
        {
            Client client = null;

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Id", id)
            };

            using (DataTable table = SQLHelper.ExecuteParamerizedSelectCommand("GetClientDetail", CommandType.StoredProcedure, parameters))
            {
                if (table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    
                    client = new Client();

                    client.Id = Convert.ToInt32(row["Id"]);
                    client.FirstName = row["FirstName"].ToString();
                    client.LastName = row["LastName"].ToString();
                    client.StreetNumber = row["StreetNumber"].ToString();
                    client.StreetName = row["StreetName"].ToString();
                    client.City = row["City"].ToString();
                    client.PostCode = row["PostCode"].ToString();
                    client.Country = row["Country"].ToString();
                }
            }

            return client;
        }


        public List<Client> GetClients()
        {
            List<Client> clients = null;
            
            using (DataTable table = SQLHelper.ExecuteSelectCommand("GetClients", CommandType.StoredProcedure))
            {
                if (table.Rows.Count > 0)
                {
                    clients = new List<Client>();
                    
                    foreach (DataRow row in table.Rows)
                    {
                        Client client = new Client();

                        client.Id = Convert.ToInt32(row["Id"]);
                        client.FirstName = row["FirstName"].ToString();
                        client.LastName = row["LastName"].ToString();
                        client.StreetNumber = row["StreetNumber"].ToString();
                        client.StreetName = row["StreetName"].ToString();
                        client.City = row["City"].ToString();
                        client.PostCode = row["PostCode"].ToString();
                        client.Country = row["Country"].ToString();

                        clients.Add(client);
                    }
                }
            }

            return clients;
        }

        public bool UpdateClient(Client client)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Id", client.Id),
                new SqlParameter("@FirstName", client.FirstName),
                new SqlParameter("@LastName", client.LastName),
                new SqlParameter("@StreetNumber", client.StreetNumber),
                new SqlParameter("@StreetName", client.StreetName),
                new SqlParameter("@City", client.City),
                new SqlParameter("@PostCode", client.PostCode),
                new SqlParameter("@Country", client.Country)
            };

            return SQLHelper.ExecuteNonQuery("UpdateClient", CommandType.StoredProcedure, parameters);
        }

        public bool DeleteClient(int id)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Id", id)

            };

            return SQLHelper.ExecuteNonQuery("DeleteClient", CommandType.StoredProcedure, parameters);
        }
    }
}
